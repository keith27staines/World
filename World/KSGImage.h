//
//  KSGImage.h
//  World
//
//  Created by Keith Staines on 05/11/2011.
//  Copyright (c) 2011 Object Computing Solutions LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSGManagedResourceBase.h"

@interface KSGImage : KSGManagedResourceBase 
{
    NSBitmapImageRep* bmp;
}


@end
